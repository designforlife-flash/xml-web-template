# AS2 & XML based Responsive Flash Web Template
![](deploy/demo.gif)

## Features
 - XML Based Configuration and Management
 - HTML Tags
 - Slideshow, News, About Us, Services, Image Gallery, Portfolio and PHP Contact Form Modules

## License
MIT